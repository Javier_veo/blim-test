package com.veo.televisa.blimtestproject;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
//        loadJSONFromAsset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        loadJSONFromAsset();
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {

            InputStream is = getActivity().getResources().openRawResource(R.raw.testasset);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.d("Json DATA",json);
        return json;

    }
}
